# Realtime Infrastructure

## Architektur

```mermaid
graph LR
HL7_ADT(HL7v2-ADT-Generator) --> M(Mirth connect)
HL7_BAR(HL7v2-ORU-Generator) --> M
HL7_ORU(HL7v2-BAR-Generator) --> M
M --> FHIR[(HAPI-FHIR)]
FHIR -->|FHIR Subscription| K(Kafka Eventstream)
K --> KSQL
KSQL --> K
K --> Spark --> R
K --> D[(Apache Druid)]-->AS(Apache Superset)
K --> S(Streamsheets)
S --> K
K --> KM(Kafka Magic)

```